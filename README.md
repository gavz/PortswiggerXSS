# PortswiggerXSS
Gathers the cheatsheet payloads and creates a usable wordlist
  
THANK YOU PORTSWIGGER FOR ALL YOUR AMAZING WORK AND EFFORT :heart:
  
Just build or run the script as is, no args needed. The rest should be handled for you.
  
Disclaimer: Not the cleanest code but it works :)  
Cheatsheet: https://portswigger.net/web-security/cross-site-scripting/cheat-sheet
